#include <Wire.h>
#include "Adafruit_TCS34725.h"
#include<Servo.h>

#define servoPin 9
#define servoPin_1 5
/* Example code for the Adafruit TCS34725 breakout library */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 3.3V DC
   Connect GROUND to common ground */

/* Initialise with default values (int time = 2.4ms, gain = 1x) */
// Adafruit_TCS34725 tcs = Adafruit_TCS34725();

/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_614MS, TCS34725_GAIN_1X);
String color;
int red,grn,blu;
 // uint16_t r, g, b, c, colorTemp, lux;
uint8_t valueR = 0;
uint8_t valueG = 0;
uint8_t valueB = 0;
Servo myservo;
Servo myservo_1;
void setup(void) {
  digitalWrite(3,HIGH);
  Serial.begin(9600);

   myservo.attach(servoPin);
   myservo_1.attach(servoPin_1);
  if (tcs.begin()) {
    Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }

 myservo.write(0);
 myservo_1.write(0);
}

void loop(void) {

  uint16_t r, g, b, c, colorTemp, lux;
 //myservo.write(0);
  tcs.getRawData(&r, &g, &b, &c);
   //colorTemp = tcs.calculateColorTemperature(r, g, b);
  colorTemp = tcs.calculateColorTemperature_dn40(r, g, b, c);
  lux = tcs.calculateLux(r, g, b);

    valueR = r >> 8;
    valueG = g >> 8;
    valueB = b >> 8;
  Serial.print("valueR: "); Serial.print(valueR, DEC); Serial.print(" ");
  Serial.print("valueG: "); Serial.print(valueG, DEC); Serial.print(" ");
  Serial.print("valueB: "); Serial.print(valueB, DEC); Serial.print(" ");
  Serial.println(" ");
    getColor();
    delay(500);
    if(color != "NO_COLOR"){
      
       if(color == "RED"){
         myservo_1.write(0);
          delay(300);
        }
 
       if(color == "YELLOW"){
         myservo_1.write(25);
          delay(300);
        }

      if(color == "GREEN"){
         myservo_1.write(50);
          delay(300);
        }
 
      if(color == "Orange"){
         myservo_1.write(75);
         delay(300);
        }
        myservo.write(55);
       
        
      }
      else 
     myservo.write(0);
      
 /*   
  Serial.print("Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(" K - ");
  Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
  Serial.print("R: "); Serial.print(r); Serial.print(" ");
  Serial.print("G: "); Serial.print(g); Serial.print(" ");
  Serial.print("B: "); Serial.print(b); Serial.print(" ");
  Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");
  Serial.println(" ");
  */
}
void getColor()

{  
     //  if (valueR > 8  && valueR < 18   &&  valueG >  9 && valueG < 19    &&  valueB > 8  && valueB < 16)   color = "WHITE";
   if (valueR > 17 && valueR < 21  &&  valueG > 15 && valueG < 19   &&  valueB > 16 && valueB < 21)  color = "PINK";
  else if (valueR > 7 && valueR < 18   &&  valueG> 6 && valueG < 18    &&  valueB > 6 && valueB < 18)   color = "RED";
  else if (valueR > 7 && valueR < 16   &&  valueG > 18 && valueG < 30    &&  valueB > 9 && valueB < 20)   color = "GREEN";
  else if (valueR > 20 && valueR < 29   &&  valueG > 24 && valueG < 38    &&  valueB > 10 && valueB < 23)   color = "YELLOW";
  else if (valueR > 17 && valueR < 26  &&  valueG > 12 && valueG < 25   &&  valueB> 9 && valueB < 22)   color = "Orange";
  else  color = "NO_COLOR";
  Serial.println(color);
}
